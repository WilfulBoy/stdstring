﻿#include <iostream>
#include <stdint.h>
#include <string>
#include <string.h>


int main()
{

	std::cout << "Enter your phrase: ";
	std::string phrase;
	std::cin >> phrase;
	std::cout << phrase << "\n";
	std::cout << phrase.length() << "\n";
	std::cout << phrase.back() << "\n";
	std::cout << phrase.front() << "\n";


	return 0;
}